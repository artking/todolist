import React from 'react';
import AddToDo from './AddToDo';
import ToDoListContainer, { STATUS } from './ToDoListContainer';
import Filters, { FILTERS } from './Filters';

export default class ToDoContainer extends React.Component {
    constructor() {
        super();
        this.state = {
            toDos: [
                {
                    name: "something 1",
                    status: STATUS.incomplete,
                },
                {
                    name: "something 2",
                    status: STATUS.completed,
                },
                {
                    name: "something 3",
                    status: STATUS.incomplete,
                },
                {
                    name: "something 4",
                    status: STATUS.incomplete,
                },
                {
                    name: "something 5",
                    status: STATUS.incomplete,
                }
            ]
        };
    }
    
    handleFilterClick = (name) => {
        this.setState({
            toDos: this.state.toDos.filter(toDo => (
                name === FILTERS.all || name === toDo.status 
            ))
        });
    }

    AddToDo = (toDo) => {
        const newToDo = {
            name: toDo,
            status: STATUS.incomplete
        };

        this.setState({
            toDos: [...this.state.toDos, newToDo]
        });
    }

    render() {
        return (
            <div className="container">
                <AddToDo handleAddToDo={this.AddToDo} />
                <ToDoListContainer
                    toDos={this.state.toDos}
                />
                <Filters 
                    handleClick={this.handleFilterClick}
                />
            </div>
        )
    }
}