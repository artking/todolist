import React from 'react';

export default class ToDo extends React.Component {
    
    handleClick = () => {
        this.props.handleClick(this.props.index);
    }

    render () {
        return (
            <li onClick={this.handleClick} className={this.props.info.status}>{this.props.info.name}</li>
        );
    }
}