import React from 'react';
import ToDo from './ToDo';
import { FILTERS } from './Filters';

export const STATUS = {
    incomplete: FILTERS.incomplete,
    completed: FILTERS.completed
}

export default class ToDoListContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            toDos: this.props.toDos
        };
    }

    toggleStatus = (status) => {
        return status === STATUS.incomplete ? STATUS.completed : STATUS.incomplete;
    }

    handleStatusChange = (id) => {
        const toDos = [...this.state.toDos];
        toDos[id]['status'] = this.toggleStatus(toDos[id]['status'])
        this.setState({
            toDos
        });
    }

    static getDerivedStateFromProps(props, state) {
        if (props.toDos.length !== state.toDos.length) {
            return { toDos: props.toDos };
        }
    }

    /*componentWillReceiveProps(nextProps) {
        if (this.nextProps.toDos !== this.props.toDos) {
            this.setState = {
                toDos: this.props.toDos
            };
        }
    }*/

    render() {
        return (
            <React.Fragment>
                <h1>ToDoList</h1>
                <ul>
                {
                    this.state.toDos.map((item, index) => (
                        <ToDo 
                            key={index} 
                            index={index} 
                            info={item}
                            handleClick={this.handleStatusChange}
                        />
                        )
                    )
                }
                </ul>
            </React.Fragment>
        );
    }
}