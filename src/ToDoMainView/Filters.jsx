import React from 'react';
import Filter from './Filter';

export const FILTERS = {
    all: 'All',
    completed: 'Completed',
    incomplete: 'Incomplete',
}

export default class Filters extends React.Component {
    constructor () {
        super();
        this.state = {
            filterOptions: [
                {
                    name: FILTERS.all,
                },
                {
                    name: FILTERS.completed,
                },
                {
                    name: FILTERS.incomplete,
                },
            ],
            current: FILTERS.all
        };
    }

    handleFilterClick = (name) => {
        this.setState({
            current: name
        });
        this.props.handleClick(name);
    }

    isFilterActive = (filter) => {
        return this.state.current === filter.name;
    }

    render () {
        return (
            <div>
                {
                    this.state.filterOptions.map((filter, index) => (
                            <Filter 
                                key={index}
                                name={filter.name}
                                active={this.isFilterActive(filter)}
                                handleClick={this.handleFilterClick}
                            />
                        )
                    )
                }
            </div>
        );
    }
}