import React from 'react';

export default class AddToDo extends React.Component {
    constructor () {
        super();
        this.state = {
            value: ''
        }
    }

    handleAddToDo = () => {
        debugger;
        this.props.handleAddToDo(this.state.value);
        this.setState({
            value: ""
        });
    }

    handleOnChange = (e) => {
        this.setState({
            value: e.target.value,
        });
    }

    render () {
        return (
            <div className="addTodoContainer">
                <button onClick={this.handleAddToDo}>AddToDo</button>
                <input value={this.state.value} onChange = {this.handleOnChange} />
            </div>
        );
    }
}