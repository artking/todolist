import React from 'react';

export default class Filter extends React.Component {
    constructor (props) {
        super(props);
    }

    render () {
        return (
            <div onClick={() => this.props.handleClick(this.props.name)}>
               { this.props.name }
            </div>
        )
    }
}